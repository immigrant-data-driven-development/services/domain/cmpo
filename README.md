# The Configuration Management Process Ontology
## 🚀 Goal
The Configuration Management Process Ontology (CMPO) aims at representing the activities, artifacts and stakeholders involved in the software Configuration Management Process. Since CMPO can be applied in the context of several SE subdomains, it describes some general notions applicable for diverse SEON concepts.

## 📕 Domain Documentation

Domain documentation can be found [here](./docs/README.md)

## ⚙️ Requirements

1. Postgresql
2. Java 17
3. Maven

## ⚙️ Stack
1. Spring Boot 3.0
2. Spring Data Rest 
3. Spring GraphQL


## 🔧 Install

1) Create a database with name cmpo with **CREATE DATABASE cmpo**.
2) Run the command to start the webservice and create table of database:

```bash
mvn Spring-boot:run
```
### Using Jar

```xml
<dependency>
  <groupId>br.nemo.immigrant.ontology.entity</groupId>
  <artifactId>cmpo</artifactId>
  <version>0.0.1-SNAPSHOT</version>
</dependency>
```




### Using Webservice

Execute docker-compose:
```bash
docker-compose up
```

## Debezium

Go to folder named *register* and performs following command to register in debezium:

```bash
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @register-sro.json
```

To delete, uses:

```bash
curl -i -X DELETE localhost:8083/connectors/sro-connector/
```


## 🔧 Usage

* Access [http://localhost:8081](http://localhost:8081) to see Swagger
* Acess [http://localhost:8081/grapiql](http://localhost:8081/grapiql) to see Graphql.

## ✒️ Team

* **[Paulo Sérgio dos Santos Júnior](paulossjunior@gmail.com)**

## 📕 Literature

remover o unique do external_id e internal id de artficat