# 📕Documentation

## 🌀 Project's Package Model
![Domain Diagram](packagediagram.png)

### 📲 Modules
* **[DesignEntities](./designentities/)** :Model with design entities
* **[ConfiguationManagementProcess](./configuationmanagementprocess/)** :Process for conducting the activities related to software configuration management, ensuring the completeness and correctness of software Configuration Items.
* **[Checkin](./checkin/)** :Activity for including new Versions of Configuration Items into a version control repository.
* **[Checkout](./checkout/)** :Activity for accessing defined versions of Configuration Items from a version control repository, usually for changing purposes.

