# 📕Documentation: Checkin

Activity for including new Versions of Configuration Items into a version control repository.

## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **Checkin** : Activity for including new Versions of Configuration Items into a version control repository.
* **CheckinConflict** : Check Conflict is an activity that verify if there are Conflicts between a Artifacts Copy in a Source Branch with a version of it in a Target Branch.
* **ResolveConflict** : Resolve Conflict is an activity that allows a Change Implementer fixing a Conflict in a Artifact Copy with Conflict
* **CommitArtifactCopy** : A Commit Artifact Copy is an activity that sends a Artifact Copy without Conflict to a Target Branch.
* **DeleteBranch** : Delete Branch is an activity that removes a Source Branch in a Source Repository.
