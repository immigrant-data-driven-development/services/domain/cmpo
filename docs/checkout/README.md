# 📕Documentation: Checkout

Activity for accessing defined versions of Configuration Items from a version control repository, usually for changing purposes.

## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **Checkout** : Activity for accessing defined versions of Configuration Items from a version control repository, usually for changing purposes.
* **BranchCreation** :  Branch Creation is an activity for creating a Branch in a Source Repository
* **BranchSwitch** : Branch Switch is an activity for switching between branches in a source repository while Artifact Checkout is an activity to create or update a Branch with Artifact Copy. 
* **ArtifactCheckout** : Artifact Checkout is an activity to create or update a Branch with Artifact Copy.
