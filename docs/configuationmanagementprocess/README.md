# 📕Documentation: ConfiguationManagementProcess

Process for conducting the activities related to software configuration management, ensuring the completeness and correctness of software Configuration Items.

## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **ChangeImplementer** : Stakeholder responsible for implementing a change in Configuration Items.
* **ConfiguationManagementProcess** : Process for conducting the activities related to software configuration management, ensuring the completeness and correctness of software Configuration Items.
* **BaselineEstablishment** : Activity for establishing a Baseline embracing a planned set of Configuration Items' Versions.
* **ChangeControl** : Activity for formally controlling the modification of Configuration Items, in a process of requesting, evaluating, changing and reviewing.
* **ChangeRequestClosing** : Activity for closing a reviewed and approved Change Request.
* **ChangeAccomplishment** : Activity for performing the authorized changes in a set of Configuration Items under version control.
* **Baseline** :  Information Item packaging a set of Configuration Items' Versions at a specific time in the product's life (for a product delivery or to establish a relevant point in the Project).
* **ArtifactCopy** : Copy of an Artifact that is under version control.
* **Conflict** : A Conflict represents a difference of content in a same region of a Artifact Copy
* **Version** : Each identified release of a Configuration Item submitted to a version control repository.
* **Branch** : A Branch is a Collective of the Artifacts in a Source Repository
* **SourceRepository** : A Source Repository is a Loaded Computer System Copy (e.g., an instance of GitLab installed in a Continuous Integration Server) whose purpose is to handle the changes of an Artifact Copy (e.g., copy of a source code)
* **ConfigurationItem** : Object whose configuration is being managed.
