# 📕Documentation: DesignEntities

Model with design entities

## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **CommmonEntity** : Entity with common attributes
