package br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.Checkin;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;

public interface CheckinRepository extends PagingAndSortingRepository<Checkin, Long>, ListCrudRepository<Checkin, Long> {
    Optional<IDProjection> findByApplicationsExternalId(String externalId);
    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);

    Boolean existsByApplicationsExternalId (String externalId);
}
