package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactType;
@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "commmit")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Commit extends Artifact implements Serializable {

  LocalDateTime commitedDate;


  @Column(columnDefinition = "TEXT")
  String name;
  
  @Column(columnDefinition = "TEXT")
  String message;
  String shortId;
  
  @Column(columnDefinition = "TEXT")
  String trailers;

  @Column(columnDefinition = "TEXT")
  String webURL;

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        Branch elem = (Branch) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "Branch {" +
         "id="+this.id+

          ", name='"+this.name+"'"+
          ", description='"+this.description+"'"+
          ", internalId='"+this.internalId+"'"+

      '}';
  }
}
