package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Branch;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;

public interface BranchRepository extends JpaRepository<Branch, Long>, PagingAndSortingRepository<Branch, Long>, ListCrudRepository<Branch, Long> {
    
    Optional<IDProjection> findByApplicationsExternalId(String externalId);

    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);

    Boolean existsByApplicationsExternalId (String externalId);

   // Optional<IDProjection> findByNameAndSourcerepositoryApplicationsExternalId(String branchName, String externalId);
}
