package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeImplementer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDOnlyProjection;
import org.springframework.data.jpa.repository.JpaRepository;
public interface ChangeImplementerRepository extends JpaRepository<ChangeImplementer, Long>, PagingAndSortingRepository<ChangeImplementer, Long>, ListCrudRepository<ChangeImplementer, Long> {

    Boolean existsByPersonEmail(String email);

    Boolean existsByPersonEmailAndProjectstakeholdersoftwareprojectSoftwareprojectName(String email,String projectName);

    Optional<IDOnlyProjection> findByPersonEmailAndProjectstakeholdersoftwareprojectSoftwareprojectName(String email,String projectName);

}
