package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Commit;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
public interface CommitRepository extends JpaRepository<Commit, Long>, PagingAndSortingRepository<Commit, Long>, ListCrudRepository<Commit, Long> {

    Optional<IDProjection> findByApplicationsExternalId(String externalId);

    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);

    Boolean existsByApplicationsExternalId (String externalId);

    /*@Modifying
    @Query("update Commit c set c.sourcerepository = :sourcerepository where c.id = :id")
    void addSourceRepository(@Param(value = "id") Long id, @Param(value = "sourcerepository") SourceRepository sourcerepository);*/
}
