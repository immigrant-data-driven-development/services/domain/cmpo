package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ConfiguationManagementProcess;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConfiguationManagementProcessRepository extends JpaRepository<ConfiguationManagementProcess, Long>, PagingAndSortingRepository<ConfiguationManagementProcess, Long>, ListCrudRepository<ConfiguationManagementProcess, Long> {

}
