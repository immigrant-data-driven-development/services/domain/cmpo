package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepositoryManager;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SourceRepositoryManagerRepository extends JpaRepository<SourceRepositoryManager, Long>, PagingAndSortingRepository<SourceRepositoryManager, Long>, ListCrudRepository<SourceRepositoryManager, Long> {

    Optional<IDProjection> findByApplicationsExternalId(String externalId);
    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);

    Boolean existsByApplicationsExternalId (String externalId);
}
