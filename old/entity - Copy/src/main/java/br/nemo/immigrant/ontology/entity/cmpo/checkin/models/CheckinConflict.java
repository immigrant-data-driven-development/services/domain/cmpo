package br.nemo.immigrant.ontology.entity.cmpo.checkin.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ArtifactCopy;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ArtifactCopy;

import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "checkinconflict")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class CheckinConflict extends Activity implements Serializable {




  @ManyToOne
  @JoinColumn(name = "checkin_id")
  private Checkin checkin;

  @ManyToMany
  @JoinTable(
      name = "checkinwithconflicts_artifactcopy",
      joinColumns = @JoinColumn(name = "checkinconflict_id"),
      inverseJoinColumns = @JoinColumn(name = "artifactcopy_id")
  )
  @Builder.Default
  private Set<ArtifactCopy> artifactWithConflict = new HashSet<>();

  @ManyToMany
  @JoinTable(
      name = "checkinwithoutconflicts_artifactcopy",
      joinColumns = @JoinColumn(name = "checkinconflict_id"),
      inverseJoinColumns = @JoinColumn(name = "artifactcopy_id")
  )
  @Builder.Default
  private Set<ArtifactCopy> artifactWithoutConflict = new HashSet<>();





  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        CheckinConflict elem = (CheckinConflict) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "CheckinConflict {" +
         "id="+this.id+


      '}';
  }
}
