package br.nemo.immigrant.ontology.entity.cmpo.checkin.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Branch;

import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "deletebranch")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class DeleteBranch extends Activity implements Serializable {




  @OneToOne(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "deletebranch")
  @Builder.Default
  private Checkin checkin = null;

  @OneToOne
  @JoinColumn(name = "branch_id", referencedColumnName = "id")
  private Branch branch;





  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        DeleteBranch elem = (DeleteBranch) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "DeleteBranch {" +
         "id="+this.id+


      '}';
  }
}
