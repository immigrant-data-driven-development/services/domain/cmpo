package br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.CheckinConflict;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CheckinConflictRepository extends PagingAndSortingRepository<CheckinConflict, Long>, ListCrudRepository<CheckinConflict, Long> {

}
