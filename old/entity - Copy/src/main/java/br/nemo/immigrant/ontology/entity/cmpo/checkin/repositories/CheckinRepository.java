package br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.Checkin;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CheckinRepository extends PagingAndSortingRepository<Checkin, Long>, ListCrudRepository<Checkin, Long> {

}
