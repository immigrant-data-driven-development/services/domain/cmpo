package br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.CommitArtifactCopy;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CommitArtifactCopyRepository extends PagingAndSortingRepository<CommitArtifactCopy, Long>, ListCrudRepository<CommitArtifactCopy, Long> {

}
