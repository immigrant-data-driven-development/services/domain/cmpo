package br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.DeleteBranch;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface DeleteBranchRepository extends PagingAndSortingRepository<DeleteBranch, Long>, ListCrudRepository<DeleteBranch, Long> {

}
