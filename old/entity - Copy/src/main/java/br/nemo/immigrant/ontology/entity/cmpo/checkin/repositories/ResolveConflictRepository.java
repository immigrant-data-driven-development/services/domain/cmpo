package br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.ResolveConflict;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ResolveConflictRepository extends PagingAndSortingRepository<ResolveConflict, Long>, ListCrudRepository<ResolveConflict, Long> {

}
