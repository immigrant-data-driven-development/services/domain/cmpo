package br.nemo.immigrant.ontology.entity.cmpo.checkout.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeImplementer;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeAccomplishment;

import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "checkout")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class Checkout extends Activity implements Serializable {




  @ManyToMany(mappedBy = "checkouts")
  @Builder.Default
  private Set<ChangeImplementer> changeimplementers = new HashSet<>();

  @ManyToOne
  @JoinColumn(name = "changeaccomplishment_id")
  private ChangeAccomplishment changeaccomplishment;

  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "checkout")
  @Builder.Default
  Set<BranchCreation> branchcreations = new HashSet<>();

  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "checkout")
  @Builder.Default
  Set<BranchSwitch> branchswitchs = new HashSet<>();

  @OneToOne
  @JoinColumn(name = "artifactcheckout_id", referencedColumnName = "id")
  private ArtifactCheckout artifactcheckout;





  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        Checkout elem = (Checkout) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "Checkout {" +
         "id="+this.id+


      '}';
  }
}
