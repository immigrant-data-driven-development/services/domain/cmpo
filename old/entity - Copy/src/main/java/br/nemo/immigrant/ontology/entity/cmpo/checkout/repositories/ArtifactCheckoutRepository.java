package br.nemo.immigrant.ontology.entity.cmpo.checkout.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkout.models.ArtifactCheckout;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ArtifactCheckoutRepository extends PagingAndSortingRepository<ArtifactCheckout, Long>, ListCrudRepository<ArtifactCheckout, Long> {

}
