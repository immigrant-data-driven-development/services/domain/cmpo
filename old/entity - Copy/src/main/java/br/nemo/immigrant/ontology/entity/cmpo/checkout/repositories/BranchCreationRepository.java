package br.nemo.immigrant.ontology.entity.cmpo.checkout.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkout.models.BranchCreation;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface BranchCreationRepository extends PagingAndSortingRepository<BranchCreation, Long>, ListCrudRepository<BranchCreation, Long> {

}
