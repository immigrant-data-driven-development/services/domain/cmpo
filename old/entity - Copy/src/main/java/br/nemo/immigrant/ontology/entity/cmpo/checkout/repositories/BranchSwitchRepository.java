package br.nemo.immigrant.ontology.entity.cmpo.checkout.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkout.models.BranchSwitch;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface BranchSwitchRepository extends PagingAndSortingRepository<BranchSwitch, Long>, ListCrudRepository<BranchSwitch, Long> {

}
