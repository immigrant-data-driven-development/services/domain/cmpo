package br.nemo.immigrant.ontology.entity.cmpo.checkout.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkout.models.Checkout;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CheckoutRepository extends PagingAndSortingRepository<Checkout, Long>, ListCrudRepository<Checkout, Long> {

}
