package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.CheckinConflict;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.CheckinConflict;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.ResolveConflict;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.CommitArtifactCopy;
import br.nemo.immigrant.ontology.entity.cmpo.checkout.models.ArtifactCheckout;

import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "artifactcopy")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class ArtifactCopy extends Artifact implements Serializable {

  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "artifactcopy")
  @Builder.Default
  Set<Version> versions = new HashSet<>();

  @ManyToMany
  @JoinTable(
      name = "artifactcopy_conflict",
      joinColumns = @JoinColumn(name = "artifactcopy_id"),
      inverseJoinColumns = @JoinColumn(name = "conflict_id")
  )
  @Builder.Default
  private Set<Conflict> conflicts = new HashSet<>();

  @ManyToOne
  @JoinColumn(name = "branch_id")
  private Branch branch;

  @ManyToMany(mappedBy = "artifactWithConflict")
  @Builder.Default
  private Set<CheckinConflict> checkinWithConflicts = new HashSet<>();

  @ManyToMany(mappedBy = "artifactWithoutConflict")
  @Builder.Default
  private Set<CheckinConflict> checkinWithoutConflicts = new HashSet<>();

  @ManyToMany(mappedBy = "artifactcopys")
  @Builder.Default
  private Set<ResolveConflict> resolveconflicts = new HashSet<>();

  @ManyToMany(mappedBy = "artifactcopys")
  @Builder.Default
  private Set<CommitArtifactCopy> commitartifactcopys = new HashSet<>();

  @ManyToMany(mappedBy = "artifactcopys")
  @Builder.Default
  private Set<ArtifactCheckout> artifactcheckouts = new HashSet<>();

  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        ArtifactCopy elem = (ArtifactCopy) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "ArtifactCopy {" +
         "id="+this.id+


      '}';
  }
}
