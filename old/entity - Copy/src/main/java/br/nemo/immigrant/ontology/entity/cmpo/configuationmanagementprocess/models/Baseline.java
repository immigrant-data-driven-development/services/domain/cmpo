package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "baseline")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class Baseline extends Artifact implements Serializable {

  @OneToOne(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "baseline")
  @Builder.Default
  private BaselineEstablishment baselineestablishment = null;

  @ManyToMany
  @JoinTable(
      name = "baseline_version",
      joinColumns = @JoinColumn(name = "baseline_id"),
      inverseJoinColumns = @JoinColumn(name = "version_id")
  )
  @Builder.Default
  private Set<Version> versions = new HashSet<>();


  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        Baseline elem = (Baseline) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "Baseline {" +
         "id="+this.id+


      '}';
  }
}
