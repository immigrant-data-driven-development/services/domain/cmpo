package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.Checkin;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.Checkin;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.DeleteBranch;
import br.nemo.immigrant.ontology.entity.cmpo.checkout.models.BranchCreation;
import br.nemo.immigrant.ontology.entity.cmpo.checkout.models.BranchSwitch;

import br.nemo.immigrant.ontology.entity.cmpo.designentities.models.CommmonEntity;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "branch")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Branch extends CommmonEntity implements Serializable {

  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "branch")
  @Builder.Default
  Set<ArtifactCopy> artifactcopys = new HashSet<>();

  @ManyToOne
  @JoinColumn(name = "sourcerepository_id")
  private SourceRepository sourcerepository;

  @OneToOne(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "sourceBranch")
  @Builder.Default
  private Checkin sourceCheckin = null;

  @OneToOne(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "targetBranch")
  @Builder.Default
  private Checkin targetCheckin = null;

  @OneToOne(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "branch")
  @Builder.Default
  private DeleteBranch deletebranch = null;

  @OneToOne(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "branch")
  @Builder.Default
  private BranchCreation branchcreation = null;

  @OneToOne(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "branch")
  @Builder.Default
  private BranchSwitch branchswitch = null;





  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        Branch elem = (Branch) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "Branch {" +
         "id="+this.id+

          ", name='"+this.name+"'"+
          ", description='"+this.description+"'"+
          ", externalId='"+this.externalId+"'"+
          ", internalId='"+this.internalId+"'"+

      '}';
  }
}
