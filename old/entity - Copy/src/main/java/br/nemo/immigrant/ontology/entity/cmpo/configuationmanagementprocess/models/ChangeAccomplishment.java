package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.Checkin;
import br.nemo.immigrant.ontology.entity.cmpo.checkout.models.Checkout;

import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "changeaccomplishment")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class ChangeAccomplishment extends Activity implements Serializable {




  @ManyToMany(mappedBy = "changeaccomplishments")
  @Builder.Default
  private Set<ChangeImplementer> changeimplementers = new HashSet<>();

  @OneToOne(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "changeaccomplishment")
  @Builder.Default
  private ChangeControl changecontrol = null;

  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "changeaccomplishment")
  @Builder.Default
  Set<Checkin> checkins = new HashSet<>();

  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "changeaccomplishment")
  @Builder.Default
  Set<Checkout> checkouts = new HashSet<>();





  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        ChangeAccomplishment elem = (ChangeAccomplishment) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "ChangeAccomplishment {" +
         "id="+this.id+


      '}';
  }
}
