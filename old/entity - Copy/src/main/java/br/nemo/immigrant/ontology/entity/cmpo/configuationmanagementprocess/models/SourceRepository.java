package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepositoryManager;

import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "sourcerepository")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class SourceRepository extends Artifact implements Serializable {

    private LocalDateTime createdDate ;

    private LocalDateTime lastUpdateDate;

    private String defaultBranchName;

    private int forkCount;

    private String readmeURL;

    private String repoURL;

    private String webURL;

    private String tag;

    private String topic;

    @ManyToOne
    @JoinColumn(name = "sourcerepositorymanager_id")
    private SourceRepositoryManager sourcerepositorymanager;



  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "sourcerepository")
  @Builder.Default
  Set<Branch> branchs = new HashSet<>();


  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        SourceRepository elem = (SourceRepository) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "SourceRepository {" +
         "id="+this.id+


      '}';
  }
}
