package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ArtifactCopy;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ArtifactCopyRepository extends PagingAndSortingRepository<ArtifactCopy, Long>, ListCrudRepository<ArtifactCopy, Long> {

}
