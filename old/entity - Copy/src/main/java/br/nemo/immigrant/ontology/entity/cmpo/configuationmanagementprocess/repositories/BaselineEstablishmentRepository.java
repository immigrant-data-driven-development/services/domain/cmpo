package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.BaselineEstablishment;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface BaselineEstablishmentRepository extends PagingAndSortingRepository<BaselineEstablishment, Long>, ListCrudRepository<BaselineEstablishment, Long> {

}
