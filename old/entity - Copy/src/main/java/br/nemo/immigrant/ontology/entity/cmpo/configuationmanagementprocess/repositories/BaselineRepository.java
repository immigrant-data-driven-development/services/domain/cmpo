package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Baseline;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface BaselineRepository extends PagingAndSortingRepository<Baseline, Long>, ListCrudRepository<Baseline, Long> {

}
