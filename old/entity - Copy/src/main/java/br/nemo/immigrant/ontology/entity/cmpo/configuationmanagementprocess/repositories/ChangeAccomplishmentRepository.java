package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeAccomplishment;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ChangeAccomplishmentRepository extends PagingAndSortingRepository<ChangeAccomplishment, Long>, ListCrudRepository<ChangeAccomplishment, Long> {

}
