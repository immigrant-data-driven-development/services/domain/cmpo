package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeControl;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ChangeControlRepository extends PagingAndSortingRepository<ChangeControl, Long>, ListCrudRepository<ChangeControl, Long> {

}
