package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeImplementer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ChangeImplementerRepository extends PagingAndSortingRepository<ChangeImplementer, Long>, ListCrudRepository<ChangeImplementer, Long> {

}
