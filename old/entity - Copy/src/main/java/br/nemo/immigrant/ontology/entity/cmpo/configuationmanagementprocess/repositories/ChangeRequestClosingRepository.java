package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeRequestClosing;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ChangeRequestClosingRepository extends PagingAndSortingRepository<ChangeRequestClosing, Long>, ListCrudRepository<ChangeRequestClosing, Long> {

}
