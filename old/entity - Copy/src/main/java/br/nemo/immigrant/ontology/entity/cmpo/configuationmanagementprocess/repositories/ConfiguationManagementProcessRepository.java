package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ConfiguationManagementProcess;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ConfiguationManagementProcessRepository extends PagingAndSortingRepository<ConfiguationManagementProcess, Long>, ListCrudRepository<ConfiguationManagementProcess, Long> {

}
