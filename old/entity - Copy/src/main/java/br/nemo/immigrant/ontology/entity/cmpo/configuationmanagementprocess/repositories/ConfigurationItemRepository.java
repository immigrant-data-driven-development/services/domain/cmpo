package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ConfigurationItem;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ConfigurationItemRepository extends PagingAndSortingRepository<ConfigurationItem, Long>, ListCrudRepository<ConfigurationItem, Long> {

}
