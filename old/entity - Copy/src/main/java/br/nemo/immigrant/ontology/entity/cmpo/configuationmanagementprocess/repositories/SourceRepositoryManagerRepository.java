package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepositoryManager;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface SourceRepositoryManagerRepository extends PagingAndSortingRepository<SourceRepositoryManager, Long>, ListCrudRepository<SourceRepositoryManager, Long> {

}
