package br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface SourceRepositoryRepository extends PagingAndSortingRepository<SourceRepository, Long>, ListCrudRepository<SourceRepository, Long> {

}
