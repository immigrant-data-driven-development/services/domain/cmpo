package br.nemo.immigrant.ontology.service.cmpo.checkin.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.CheckinConflict;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories.CheckinConflictRepository;
import br.nemo.immigrant.ontology.service.cmpo.checkin.records.CheckinConflictInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CheckinConflictController  {

  @Autowired
  CheckinConflictRepository repository;

  @QueryMapping
  public List<CheckinConflict> findAllCheckinConflicts() {
    return repository.findAll();
  }

  @QueryMapping
  public CheckinConflict findByIDCheckinConflict(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public CheckinConflict createCheckinConflict(@Argument CheckinConflictInput input) {
    CheckinConflict instance = CheckinConflict.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public CheckinConflict updateCheckinConflict(@Argument Long id, @Argument CheckinConflictInput input) {
    CheckinConflict instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("CheckinConflict not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCheckinConflict(@Argument Long id) {
    repository.deleteById(id);
  }

}
