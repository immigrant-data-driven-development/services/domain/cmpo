package br.nemo.immigrant.ontology.service.cmpo.checkin.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.CommitArtifactCopy;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories.CommitArtifactCopyRepository;
import br.nemo.immigrant.ontology.service.cmpo.checkin.records.CommitArtifactCopyInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CommitArtifactCopyController  {

  @Autowired
  CommitArtifactCopyRepository repository;

  @QueryMapping
  public List<CommitArtifactCopy> findAllCommitArtifactCopys() {
    return repository.findAll();
  }

  @QueryMapping
  public CommitArtifactCopy findByIDCommitArtifactCopy(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public CommitArtifactCopy createCommitArtifactCopy(@Argument CommitArtifactCopyInput input) {
    CommitArtifactCopy instance = CommitArtifactCopy.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public CommitArtifactCopy updateCommitArtifactCopy(@Argument Long id, @Argument CommitArtifactCopyInput input) {
    CommitArtifactCopy instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("CommitArtifactCopy not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCommitArtifactCopy(@Argument Long id) {
    repository.deleteById(id);
  }

}
