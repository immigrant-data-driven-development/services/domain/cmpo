package br.nemo.immigrant.ontology.service.cmpo.checkin.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.DeleteBranch;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories.DeleteBranchRepository;
import br.nemo.immigrant.ontology.service.cmpo.checkin.records.DeleteBranchInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class DeleteBranchController  {

  @Autowired
  DeleteBranchRepository repository;

  @QueryMapping
  public List<DeleteBranch> findAllDeleteBranchs() {
    return repository.findAll();
  }

  @QueryMapping
  public DeleteBranch findByIDDeleteBranch(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public DeleteBranch createDeleteBranch(@Argument DeleteBranchInput input) {
    DeleteBranch instance = DeleteBranch.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public DeleteBranch updateDeleteBranch(@Argument Long id, @Argument DeleteBranchInput input) {
    DeleteBranch instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("DeleteBranch not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteDeleteBranch(@Argument Long id) {
    repository.deleteById(id);
  }

}
