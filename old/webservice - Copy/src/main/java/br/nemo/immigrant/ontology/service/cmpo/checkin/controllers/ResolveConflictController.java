package br.nemo.immigrant.ontology.service.cmpo.checkin.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.ResolveConflict;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories.ResolveConflictRepository;
import br.nemo.immigrant.ontology.service.cmpo.checkin.records.ResolveConflictInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ResolveConflictController  {

  @Autowired
  ResolveConflictRepository repository;

  @QueryMapping
  public List<ResolveConflict> findAllResolveConflicts() {
    return repository.findAll();
  }

  @QueryMapping
  public ResolveConflict findByIDResolveConflict(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ResolveConflict createResolveConflict(@Argument ResolveConflictInput input) {
    ResolveConflict instance = ResolveConflict.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ResolveConflict updateResolveConflict(@Argument Long id, @Argument ResolveConflictInput input) {
    ResolveConflict instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ResolveConflict not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteResolveConflict(@Argument Long id) {
    repository.deleteById(id);
  }

}
