package br.nemo.immigrant.ontology.service.cmpo.checkin.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.CheckinConflict;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories.CheckinConflictRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "checkinconflict", path = "checkinconflict")
public interface CheckinConflictRepositoryWeb extends CheckinConflictRepository {

}
