package br.nemo.immigrant.ontology.service.cmpo.checkin.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.Checkin;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories.CheckinRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "checkin", path = "checkin")
public interface CheckinRepositoryWeb extends CheckinRepository {

}
