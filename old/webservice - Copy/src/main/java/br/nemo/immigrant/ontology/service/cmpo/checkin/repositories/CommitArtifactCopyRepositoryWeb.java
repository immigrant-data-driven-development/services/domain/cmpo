package br.nemo.immigrant.ontology.service.cmpo.checkin.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.CommitArtifactCopy;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories.CommitArtifactCopyRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "commitartifactcopy", path = "commitartifactcopy")
public interface CommitArtifactCopyRepositoryWeb extends CommitArtifactCopyRepository {

}
