package br.nemo.immigrant.ontology.service.cmpo.checkin.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.DeleteBranch;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories.DeleteBranchRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "deletebranch", path = "deletebranch")
public interface DeleteBranchRepositoryWeb extends DeleteBranchRepository {

}
