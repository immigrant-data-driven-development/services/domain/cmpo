package br.nemo.immigrant.ontology.service.cmpo.checkin.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.ResolveConflict;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories.ResolveConflictRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "resolveconflict", path = "resolveconflict")
public interface ResolveConflictRepositoryWeb extends ResolveConflictRepository {

}
