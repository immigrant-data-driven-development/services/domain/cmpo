package br.nemo.immigrant.ontology.service.cmpo.checkout.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.checkout.models.ArtifactCheckout;
import br.nemo.immigrant.ontology.entity.cmpo.checkout.repositories.ArtifactCheckoutRepository;
import br.nemo.immigrant.ontology.service.cmpo.checkout.records.ArtifactCheckoutInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ArtifactCheckoutController  {

  @Autowired
  ArtifactCheckoutRepository repository;

  @QueryMapping
  public List<ArtifactCheckout> findAllArtifactCheckouts() {
    return repository.findAll();
  }

  @QueryMapping
  public ArtifactCheckout findByIDArtifactCheckout(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ArtifactCheckout createArtifactCheckout(@Argument ArtifactCheckoutInput input) {
    ArtifactCheckout instance = ArtifactCheckout.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ArtifactCheckout updateArtifactCheckout(@Argument Long id, @Argument ArtifactCheckoutInput input) {
    ArtifactCheckout instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ArtifactCheckout not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteArtifactCheckout(@Argument Long id) {
    repository.deleteById(id);
  }

}
