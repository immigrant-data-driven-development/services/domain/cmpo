package br.nemo.immigrant.ontology.service.cmpo.checkout.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.checkout.models.BranchCreation;
import br.nemo.immigrant.ontology.entity.cmpo.checkout.repositories.BranchCreationRepository;
import br.nemo.immigrant.ontology.service.cmpo.checkout.records.BranchCreationInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class BranchCreationController  {

  @Autowired
  BranchCreationRepository repository;

  @QueryMapping
  public List<BranchCreation> findAllBranchCreations() {
    return repository.findAll();
  }

  @QueryMapping
  public BranchCreation findByIDBranchCreation(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public BranchCreation createBranchCreation(@Argument BranchCreationInput input) {
    BranchCreation instance = BranchCreation.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public BranchCreation updateBranchCreation(@Argument Long id, @Argument BranchCreationInput input) {
    BranchCreation instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("BranchCreation not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteBranchCreation(@Argument Long id) {
    repository.deleteById(id);
  }

}
