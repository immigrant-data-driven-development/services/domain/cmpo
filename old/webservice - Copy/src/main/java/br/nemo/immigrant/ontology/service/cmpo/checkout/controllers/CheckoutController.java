package br.nemo.immigrant.ontology.service.cmpo.checkout.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.checkout.models.Checkout;
import br.nemo.immigrant.ontology.entity.cmpo.checkout.repositories.CheckoutRepository;
import br.nemo.immigrant.ontology.service.cmpo.checkout.records.CheckoutInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CheckoutController  {

  @Autowired
  CheckoutRepository repository;

  @QueryMapping
  public List<Checkout> findAllCheckouts() {
    return repository.findAll();
  }

  @QueryMapping
  public Checkout findByIDCheckout(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Checkout createCheckout(@Argument CheckoutInput input) {
    Checkout instance = Checkout.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public Checkout updateCheckout(@Argument Long id, @Argument CheckoutInput input) {
    Checkout instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Checkout not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCheckout(@Argument Long id) {
    repository.deleteById(id);
  }

}
