package br.nemo.immigrant.ontology.service.cmpo.checkout.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkout.models.ArtifactCheckout;
import br.nemo.immigrant.ontology.entity.cmpo.checkout.repositories.ArtifactCheckoutRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "artifactcheckout", path = "artifactcheckout")
public interface ArtifactCheckoutRepositoryWeb extends ArtifactCheckoutRepository {

}
