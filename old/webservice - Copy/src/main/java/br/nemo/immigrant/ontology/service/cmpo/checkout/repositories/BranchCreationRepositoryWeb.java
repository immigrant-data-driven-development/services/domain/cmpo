package br.nemo.immigrant.ontology.service.cmpo.checkout.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkout.models.BranchCreation;
import br.nemo.immigrant.ontology.entity.cmpo.checkout.repositories.BranchCreationRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "branchcreation", path = "branchcreation")
public interface BranchCreationRepositoryWeb extends BranchCreationRepository {

}
