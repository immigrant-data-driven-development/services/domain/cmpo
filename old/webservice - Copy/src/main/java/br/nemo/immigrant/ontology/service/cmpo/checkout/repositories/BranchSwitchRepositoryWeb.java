package br.nemo.immigrant.ontology.service.cmpo.checkout.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkout.models.BranchSwitch;
import br.nemo.immigrant.ontology.entity.cmpo.checkout.repositories.BranchSwitchRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "branchswitch", path = "branchswitch")
public interface BranchSwitchRepositoryWeb extends BranchSwitchRepository {

}
