package br.nemo.immigrant.ontology.service.cmpo.checkout.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.checkout.models.Checkout;
import br.nemo.immigrant.ontology.entity.cmpo.checkout.repositories.CheckoutRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "checkout", path = "checkout")
public interface CheckoutRepositoryWeb extends CheckoutRepository {

}
