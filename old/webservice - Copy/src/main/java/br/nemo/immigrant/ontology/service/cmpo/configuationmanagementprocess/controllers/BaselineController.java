package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Baseline;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.BaselineRepository;
import br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records.BaselineInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class BaselineController  {

  @Autowired
  BaselineRepository repository;

  @QueryMapping
  public List<Baseline> findAllBaselines() {
    return repository.findAll();
  }

  @QueryMapping
  public Baseline findByIDBaseline(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Baseline createBaseline(@Argument BaselineInput input) {
    Baseline instance = Baseline.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public Baseline updateBaseline(@Argument Long id, @Argument BaselineInput input) {
    Baseline instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Baseline not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteBaseline(@Argument Long id) {
    repository.deleteById(id);
  }

}
