package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.BaselineEstablishment;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.BaselineEstablishmentRepository;
import br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records.BaselineEstablishmentInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class BaselineEstablishmentController  {

  @Autowired
  BaselineEstablishmentRepository repository;

  @QueryMapping
  public List<BaselineEstablishment> findAllBaselineEstablishments() {
    return repository.findAll();
  }

  @QueryMapping
  public BaselineEstablishment findByIDBaselineEstablishment(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public BaselineEstablishment createBaselineEstablishment(@Argument BaselineEstablishmentInput input) {
    BaselineEstablishment instance = BaselineEstablishment.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public BaselineEstablishment updateBaselineEstablishment(@Argument Long id, @Argument BaselineEstablishmentInput input) {
    BaselineEstablishment instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("BaselineEstablishment not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteBaselineEstablishment(@Argument Long id) {
    repository.deleteById(id);
  }

}
