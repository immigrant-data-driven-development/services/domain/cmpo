package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Branch;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.BranchRepository;
import br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records.BranchInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class BranchController  {

  @Autowired
  BranchRepository repository;

  @QueryMapping
  public List<Branch> findAllBranchs() {
    return repository.findAll();
  }

  @QueryMapping
  public Branch findByIDBranch(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Branch createBranch(@Argument BranchInput input) {
    Branch instance = Branch.builder().name(input.name()).
                                       description(input.description()).
                                       externalId(input.externalId()).
                                       internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public Branch updateBranch(@Argument Long id, @Argument BranchInput input) {
    Branch instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Branch not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setExternalId(input.externalId());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteBranch(@Argument Long id) {
    repository.deleteById(id);
  }

}
