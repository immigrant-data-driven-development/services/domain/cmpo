package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeAccomplishment;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ChangeAccomplishmentRepository;
import br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records.ChangeAccomplishmentInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ChangeAccomplishmentController  {

  @Autowired
  ChangeAccomplishmentRepository repository;

  @QueryMapping
  public List<ChangeAccomplishment> findAllChangeAccomplishments() {
    return repository.findAll();
  }

  @QueryMapping
  public ChangeAccomplishment findByIDChangeAccomplishment(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ChangeAccomplishment createChangeAccomplishment(@Argument ChangeAccomplishmentInput input) {
    ChangeAccomplishment instance = ChangeAccomplishment.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ChangeAccomplishment updateChangeAccomplishment(@Argument Long id, @Argument ChangeAccomplishmentInput input) {
    ChangeAccomplishment instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ChangeAccomplishment not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteChangeAccomplishment(@Argument Long id) {
    repository.deleteById(id);
  }

}
