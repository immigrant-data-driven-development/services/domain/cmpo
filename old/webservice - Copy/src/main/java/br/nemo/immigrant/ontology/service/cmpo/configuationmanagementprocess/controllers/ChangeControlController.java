package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeControl;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ChangeControlRepository;
import br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records.ChangeControlInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ChangeControlController  {

  @Autowired
  ChangeControlRepository repository;

  @QueryMapping
  public List<ChangeControl> findAllChangeControls() {
    return repository.findAll();
  }

  @QueryMapping
  public ChangeControl findByIDChangeControl(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ChangeControl createChangeControl(@Argument ChangeControlInput input) {
    ChangeControl instance = ChangeControl.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ChangeControl updateChangeControl(@Argument Long id, @Argument ChangeControlInput input) {
    ChangeControl instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ChangeControl not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteChangeControl(@Argument Long id) {
    repository.deleteById(id);
  }

}
