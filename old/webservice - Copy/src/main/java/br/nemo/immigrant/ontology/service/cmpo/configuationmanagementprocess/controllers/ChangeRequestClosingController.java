package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeRequestClosing;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ChangeRequestClosingRepository;
import br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records.ChangeRequestClosingInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ChangeRequestClosingController  {

  @Autowired
  ChangeRequestClosingRepository repository;

  @QueryMapping
  public List<ChangeRequestClosing> findAllChangeRequestClosings() {
    return repository.findAll();
  }

  @QueryMapping
  public ChangeRequestClosing findByIDChangeRequestClosing(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ChangeRequestClosing createChangeRequestClosing(@Argument ChangeRequestClosingInput input) {
    ChangeRequestClosing instance = ChangeRequestClosing.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ChangeRequestClosing updateChangeRequestClosing(@Argument Long id, @Argument ChangeRequestClosingInput input) {
    ChangeRequestClosing instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ChangeRequestClosing not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteChangeRequestClosing(@Argument Long id) {
    repository.deleteById(id);
  }

}
