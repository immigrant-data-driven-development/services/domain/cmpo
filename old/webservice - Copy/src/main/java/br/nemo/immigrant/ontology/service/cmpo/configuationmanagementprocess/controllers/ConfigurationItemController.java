package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ConfigurationItem;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ConfigurationItemRepository;
import br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records.ConfigurationItemInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ConfigurationItemController  {

  @Autowired
  ConfigurationItemRepository repository;

  @QueryMapping
  public List<ConfigurationItem> findAllConfigurationItems() {
    return repository.findAll();
  }

  @QueryMapping
  public ConfigurationItem findByIDConfigurationItem(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ConfigurationItem createConfigurationItem(@Argument ConfigurationItemInput input) {
    ConfigurationItem instance = ConfigurationItem.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ConfigurationItem updateConfigurationItem(@Argument Long id, @Argument ConfigurationItemInput input) {
    ConfigurationItem instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ConfigurationItem not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteConfigurationItem(@Argument Long id) {
    repository.deleteById(id);
  }

}
