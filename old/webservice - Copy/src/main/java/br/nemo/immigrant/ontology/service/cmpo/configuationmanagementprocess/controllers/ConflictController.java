package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Conflict;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ConflictRepository;
import br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records.ConflictInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ConflictController  {

  @Autowired
  ConflictRepository repository;

  @QueryMapping
  public List<Conflict> findAllConflicts() {
    return repository.findAll();
  }

  @QueryMapping
  public Conflict findByIDConflict(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Conflict createConflict(@Argument ConflictInput input) {
    Conflict instance = Conflict.builder().name(input.name()).
                                           description(input.description()).
                                           externalId(input.externalId()).
                                           internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public Conflict updateConflict(@Argument Long id, @Argument ConflictInput input) {
    Conflict instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Conflict not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setExternalId(input.externalId());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteConflict(@Argument Long id) {
    repository.deleteById(id);
  }

}
