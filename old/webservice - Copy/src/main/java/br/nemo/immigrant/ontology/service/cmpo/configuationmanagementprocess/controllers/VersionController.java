package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Version;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.VersionRepository;
import br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records.VersionInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class VersionController  {

  @Autowired
  VersionRepository repository;

  @QueryMapping
  public List<Version> findAllVersions() {
    return repository.findAll();
  }

  @QueryMapping
  public Version findByIDVersion(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Version createVersion(@Argument VersionInput input) {
    Version instance = Version.builder().name(input.name()).
                                         description(input.description()).
                                         externalId(input.externalId()).
                                         internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public Version updateVersion(@Argument Long id, @Argument VersionInput input) {
    Version instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Version not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setExternalId(input.externalId());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteVersion(@Argument Long id) {
    repository.deleteById(id);
  }

}
