package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records;
import java.time.LocalDate;
public record ConflictInput( String name,String description,String externalId,String internalId ) {
}
