package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records;
import java.time.LocalDate;
public record VersionInput( String name,String description,String externalId,String internalId ) {
}
