package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ArtifactCopy;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ArtifactCopyRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "artifactcopy", path = "artifactcopy")
public interface ArtifactCopyRepositoryWeb extends ArtifactCopyRepository {

}
