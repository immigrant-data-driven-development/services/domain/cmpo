package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.BaselineEstablishment;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.BaselineEstablishmentRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "baselineestablishment", path = "baselineestablishment")
public interface BaselineEstablishmentRepositoryWeb extends BaselineEstablishmentRepository {

}
