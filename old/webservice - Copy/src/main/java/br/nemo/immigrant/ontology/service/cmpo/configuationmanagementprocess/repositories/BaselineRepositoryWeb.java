package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Baseline;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.BaselineRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "baseline", path = "baseline")
public interface BaselineRepositoryWeb extends BaselineRepository {

}
