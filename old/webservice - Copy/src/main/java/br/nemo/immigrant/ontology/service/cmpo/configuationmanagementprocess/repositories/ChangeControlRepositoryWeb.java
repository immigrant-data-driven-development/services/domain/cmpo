package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeControl;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ChangeControlRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "changecontrol", path = "changecontrol")
public interface ChangeControlRepositoryWeb extends ChangeControlRepository {

}
