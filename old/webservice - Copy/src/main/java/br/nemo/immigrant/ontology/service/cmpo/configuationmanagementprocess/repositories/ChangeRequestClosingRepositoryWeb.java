package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeRequestClosing;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ChangeRequestClosingRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "changerequestclosing", path = "changerequestclosing")
public interface ChangeRequestClosingRepositoryWeb extends ChangeRequestClosingRepository {

}
