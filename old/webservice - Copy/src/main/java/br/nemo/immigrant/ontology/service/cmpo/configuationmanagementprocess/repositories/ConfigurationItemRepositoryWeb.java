package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ConfigurationItem;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ConfigurationItemRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "configurationitem", path = "configurationitem")
public interface ConfigurationItemRepositoryWeb extends ConfigurationItemRepository {

}
