package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Conflict;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ConflictRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "conflict", path = "conflict")
public interface ConflictRepositoryWeb extends ConflictRepository {

}
