package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Version;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.VersionRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "version", path = "version")
public interface VersionRepositoryWeb extends VersionRepository {

}
