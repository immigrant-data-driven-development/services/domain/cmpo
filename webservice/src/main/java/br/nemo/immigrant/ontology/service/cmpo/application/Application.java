package br.nemo.immigrant.ontology.service.cmpo.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = {"br.nemo.immigrant.ontology.service.cmpo.*"})
@EntityScan(basePackages = {"br.nemo.immigrant.ontology.entity.*"})
@EnableJpaRepositories(basePackages = {"br.nemo.immigrant.ontology.service.cmpo.*"})
@OpenAPIDefinition(info = @Info(
  title = "Configuration Management Process Ontology WebService",
  version = "1.0",
  description = "The Configuration Management Process Ontology (CMPO) aims at representing the activities, artifacts and stakeholders involved in the software Configuration Management Process. Since CMPO can be applied in the context of several SE subdomains, it describes some general notions applicable for diverse SEON concepts."))

public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
