package br.nemo.immigrant.ontology.service.cmpo.checkin.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.Checkin;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories.CheckinRepository;
import br.nemo.immigrant.ontology.service.cmpo.checkin.records.CheckinInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CheckinController  {

  @Autowired
  CheckinRepository repository;

  @QueryMapping
  public List<Checkin> findAllCheckins() {
    return repository.findAll();
  }

  @QueryMapping
  public Checkin findByIDCheckin(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Checkin createCheckin(@Argument CheckinInput input) {
    Checkin instance = Checkin.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public Checkin updateCheckin(@Argument Long id, @Argument CheckinInput input) {
    Checkin instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Checkin not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCheckin(@Argument Long id) {
    repository.deleteById(id);
  }

}
