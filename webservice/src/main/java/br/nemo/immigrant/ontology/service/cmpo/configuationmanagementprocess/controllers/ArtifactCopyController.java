package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ArtifactCopy;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ArtifactCopyRepository;
import br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records.ArtifactCopyInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ArtifactCopyController  {

  @Autowired
  ArtifactCopyRepository repository;

  @QueryMapping
  public List<ArtifactCopy> findAllArtifactCopys() {
    return repository.findAll();
  }

  @QueryMapping
  public ArtifactCopy findByIDArtifactCopy(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ArtifactCopy createArtifactCopy(@Argument ArtifactCopyInput input) {
    ArtifactCopy instance = ArtifactCopy.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ArtifactCopy updateArtifactCopy(@Argument Long id, @Argument ArtifactCopyInput input) {
    ArtifactCopy instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ArtifactCopy not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteArtifactCopy(@Argument Long id) {
    repository.deleteById(id);
  }

}
