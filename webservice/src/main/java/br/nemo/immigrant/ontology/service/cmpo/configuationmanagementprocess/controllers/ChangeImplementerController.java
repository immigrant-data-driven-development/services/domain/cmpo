package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeImplementer;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ChangeImplementerRepository;
import br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records.ChangeImplementerInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ChangeImplementerController  {

  @Autowired
  ChangeImplementerRepository repository;

  @QueryMapping
  public List<ChangeImplementer> findAllChangeImplementers() {
    return repository.findAll();
  }

  @QueryMapping
  public ChangeImplementer findByIDChangeImplementer(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ChangeImplementer createChangeImplementer(@Argument ChangeImplementerInput input) {
    ChangeImplementer instance = ChangeImplementer.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ChangeImplementer updateChangeImplementer(@Argument Long id, @Argument ChangeImplementerInput input) {
    ChangeImplementer instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ChangeImplementer not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteChangeImplementer(@Argument Long id) {
    repository.deleteById(id);
  }

}
