package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Commit;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.CommitRepository;
import br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records.CommitInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CommitController  {

  @Autowired
  CommitRepository repository;

  @QueryMapping
  public List<Commit> findAllCommits() {
    return repository.findAll();
  }

  @QueryMapping
  public Commit findByIDCommit(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Commit createCommit(@Argument CommitInput input) {
    Commit instance = Commit.builder().name(input.name()).
                                       description(input.description()).
                                       internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public Commit updateCommit(@Argument Long id, @Argument CommitInput input) {
    Commit instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Commit not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCommit(@Argument Long id) {
    repository.deleteById(id);
  }

}
