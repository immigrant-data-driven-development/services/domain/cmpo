package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ConfiguationManagementProcess;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ConfiguationManagementProcessRepository;
import br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records.ConfiguationManagementProcessInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ConfiguationManagementProcessController  {

  @Autowired
  ConfiguationManagementProcessRepository repository;

  @QueryMapping
  public List<ConfiguationManagementProcess> findAllConfiguationManagementProcesss() {
    return repository.findAll();
  }

  @QueryMapping
  public ConfiguationManagementProcess findByIDConfiguationManagementProcess(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ConfiguationManagementProcess createConfiguationManagementProcess(@Argument ConfiguationManagementProcessInput input) {
    ConfiguationManagementProcess instance = ConfiguationManagementProcess.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ConfiguationManagementProcess updateConfiguationManagementProcess(@Argument Long id, @Argument ConfiguationManagementProcessInput input) {
    ConfiguationManagementProcess instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ConfiguationManagementProcess not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteConfiguationManagementProcess(@Argument Long id) {
    repository.deleteById(id);
  }

}
