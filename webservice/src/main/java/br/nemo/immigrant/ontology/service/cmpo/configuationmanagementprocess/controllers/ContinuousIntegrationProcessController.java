package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ContinuousIntegrationProcess;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ContinuousIntegrationProcessRepository;
import br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records.ContinuousIntegrationProcessInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ContinuousIntegrationProcessController  {

  @Autowired
  ContinuousIntegrationProcessRepository repository;

  @QueryMapping
  public List<ContinuousIntegrationProcess> findAllContinuousIntegrationProcesss() {
    return repository.findAll();
  }


  @QueryMapping
  public ContinuousIntegrationProcess findByIDContinuousIntegrationProcess(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }
  @MutationMapping
  public ContinuousIntegrationProcess createContinuousIntegrationProcess(@Argument ContinuousIntegrationProcessInput input) {
    ContinuousIntegrationProcess instance = ContinuousIntegrationProcess.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ContinuousIntegrationProcess updateContinuousIntegrationProcess(@Argument Long id, @Argument ContinuousIntegrationProcessInput input) {
    ContinuousIntegrationProcess instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ConfiguationManagementProcess not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteContinuousIntegrationProcess(@Argument Long id) {
    repository.deleteById(id);
  }

}
