package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.controllers;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepositoryManager;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.SourceRepositoryManagerRepository;
import br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.records.SourceRepositoryManagerInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class SourceRepositoryManagerController  {

  @Autowired
  SourceRepositoryManagerRepository repository;

  @QueryMapping
  public List<SourceRepositoryManager> findAllSourceRepositoryManagers() {
    return repository.findAll();
  }

  @QueryMapping
  public SourceRepositoryManager findByIDSourceRepositoryManager(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public SourceRepositoryManager createSourceRepositoryManager(@Argument SourceRepositoryManagerInput input) {
    SourceRepositoryManager instance = SourceRepositoryManager.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public SourceRepositoryManager updateSourceRepositoryManager(@Argument Long id, @Argument SourceRepositoryManagerInput input) {
    SourceRepositoryManager instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("SourceRepositoryManager not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteSourceRepositoryManager(@Argument Long id) {
    repository.deleteById(id);
  }

}
