package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Branch;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.BranchRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "branch", path = "branch")
public interface BranchRepositoryWeb extends BranchRepository {

}
