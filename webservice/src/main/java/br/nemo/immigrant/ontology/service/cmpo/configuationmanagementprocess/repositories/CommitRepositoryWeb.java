package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Commit;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.CommitRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "commit", path = "commit")
public interface CommitRepositoryWeb extends CommitRepository {

}
