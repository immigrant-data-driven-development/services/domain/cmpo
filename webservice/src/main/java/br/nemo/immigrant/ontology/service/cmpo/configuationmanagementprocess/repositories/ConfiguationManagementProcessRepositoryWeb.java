package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ConfiguationManagementProcess;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ConfiguationManagementProcessRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "configuationmanagementprocess", path = "configuationmanagementprocess")
public interface ConfiguationManagementProcessRepositoryWeb extends ConfiguationManagementProcessRepository {

}
