package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ContinuousIntegrationProcess;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ContinuousIntegrationProcessRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "continuousIntegrationprocess", path = "continuousIntegrationprocess")
public interface ContinuousIntegrationProcessRepositoryWeb extends ContinuousIntegrationProcessRepository {

}
