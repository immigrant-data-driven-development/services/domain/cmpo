package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepositoryManager;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.SourceRepositoryManagerRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "sourcerepositorymanager", path = "sourcerepositorymanager")
public interface SourceRepositoryManagerRepositoryWeb extends SourceRepositoryManagerRepository {

}
