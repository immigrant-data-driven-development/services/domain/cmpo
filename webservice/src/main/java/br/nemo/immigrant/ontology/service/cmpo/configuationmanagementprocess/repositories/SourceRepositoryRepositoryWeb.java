package br.nemo.immigrant.ontology.service.cmpo.configuationmanagementprocess.repositories;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepository;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.SourceRepositoryRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "sourcerepository", path = "sourcerepository")
public interface SourceRepositoryRepositoryWeb extends SourceRepositoryRepository {

}
